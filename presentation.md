---
title: LivePose Workshop
author: Christian Frisson. Gabriel Downs, Emmanuel Durand
institute: Société des Arts Technologiques [SAT]
separator: <!--s-->
verticalSeparator: <!--v-->
theme: moon 
revealOptions:
  transition: 'none'
--- 
<section id="header">
    <h1 data-i18n="title">LivePose</h1>
    <p data-i18n="subtitle">Real-time Pose Estimation for Video Streams</p>

[https://gitlab.com/sat-metalab/livepose](https://gitlab.com/sat-metalab/livepose)

<small>Christian Frisson, Gabriel Downs, Emmanuel Durand, Michał Seta</small>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-metalab.gitlab.io/)

<div class="langchooser">
<label>
<input type="radio" name="menulangradios" value="en" checked><span>en</span>
</label>
<label>
<input type="radio" name="menulangradios" value="fr"><span>fr</span>
</label>
</div>

</section>

---

<section id="livepose-experimentations">
  <h2 data-i18n="title">Interaction for Immersion</h2>
  <iframe src="https://player.vimeo.com/video/639532760" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
</section>


---

<section id="satosphere">
  <h2 data-i18n="title">In The Satosphere</h2>
  <iframe src="images/livepose/openpose_walk_0.mp4" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
</section>


---

<section id="livepose-detection">
  <h2 data-i18n="title">Action and Gesture Detection</h2>
  <p data-i18n="paragraph1">Control an interactive environment with gestures like raising arms, jumping, etc.</p>

![](images/livepose/arms_up.png)

</section>

---

<section id="pose-estimation">
    <h2 data-i18n="title">Pose estimation</h2>
    <p data-i18n="paragraph1">Goal: To estimate the pose of a person in an image by locating special body points (keypoints)</p>

![](images/livepose/keypoints_and_people.png)

</section>


---

<section id="livepose-models">
  <h2 data-i18n="title">Pose Estimation models</h2>
  <ul>
  <li data-i18n="PoseNet"><a href='https://sat-metalab.gitlab.io/livepose/en/Config.html#posenet-params'>PoseNet</a>: lightweight and fast, can run on mobile and embedded devices.</li>
  <li data-i18n="OpenPose"><a href='https://sat-metalab.gitlab.io/livepose/en/Config.html#openpose-params'>OpenPose</a>: High accuracy, for use with dedicated GPUs.</li>
  <li data-i18n="more">...and others in progress under benchmark and review.</li>
  </ul>
</section>

---

<section id="livepose-features">
  <h2 data-i18n="title">LivePose Features</h2>
  <ul>
  <li data-i18n="paragraph1">Easy to use in many different <a href='https://sat-metalab.gitlab.io/livepose/en/Config.html'>configurations</a> (with/without GPU).</li>
  <li data-i18n="paragraph2">Easy to <a href='https://sat-metalab.gitlab.io/livepose/en/Installation.html'>install</a> on Ubuntu 20.04 and Nvidia Jetsons!</li>
  </ul>
</section>


---

<section id="livepose-outputs">
  <h2 data-i18n="title">Output formats available</h2>
  <ul>
  <li data-i18n="osc"><a href='https://sat-metalab.gitlab.io/livepose/en/Config.html#osc'>OSC</a> (Open Sound Control): for sound and lighting interactive environments</li>
  <li data-i18n="websocket"><a href='https://sat-metalab.gitlab.io/livepose/en/Config.html#websocket'>WebSocket</a>: for web applications</li>
  </ul>
  <iframe src="https://player.vimeo.com/video/604196712?h=d4ab3f4b2a#&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
</section>

---

<section id="livepose-demo">
  <h2 data-i18n="title">Quick-start demo</h2>

[LivePose](https://gitlab.com/sat-metalab/livepose/) +
[Chataigne](https://github.com/benkuper/Chataigne) +
[SATIE](https://gitlab.com/sat-metalab/satie/)

  <iframe src="https://player.vimeo.com/video/639244396" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
</section>

